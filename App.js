import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ImageBackground, Pressable, Linking } from 'react-native';
const gap = 16;

export default function App() {
  return (
    <View style={styles.container}>
      <ImageBackground source={require('./assets/bg.jpeg')} resizeMode="cover" style={styles.image}>
        <View style={{ flex: 1, paddingVertical: (gap / -2), backgroundColor: 'rgba(0, 0, 0, .5)', justifyContent: 'center', paddingHorizontal: 10 }}>
          <Pressable style={{ marginVertical: gap / 2, borderRadius: 4, backgroundColor: '#FFF', padding: 8 }} title='Test' onPress={async () => {
            await Linking.openURL('https://forms.gle/SD1kpaYrNwu2D2DE9');
          }}>
            <Text style={{ textAlign: 'center' }}>
              Kesiapan Kerja Harian (KKH)
            </Text>
          </Pressable>
          <Pressable style={{ marginVertical: gap / 2, borderRadius: 4, backgroundColor: '#FFF', padding: 8 }} title='Test' onPress={async () => {
            await Linking.openURL('https://forms.gle/ei39sNWcC5N1n5zR9');
          }}>
            <Text style={{ textAlign: 'center' }}>
              Hazard Report
            </Text>
          </Pressable>
          <Pressable style={{ marginVertical: gap / 2, borderRadius: 4, backgroundColor: '#FFF', padding: 8 }} title='Test' onPress={async () => {
            await Linking.openURL('https://forms.gle/BAghhb8Bn2cWTqSv6');
          }}>
            <Text style={{ textAlign: 'center' }}>
              Analisa Kerja Aman/Job Safety Analysis
            </Text>
          </Pressable>
          <Pressable style={{ marginVertical: gap / 2, borderRadius: 4, backgroundColor: '#FFF', padding: 8 }} title='Test' onPress={async () => {
            await Linking.openURL('https://drive.google.com/drive/folders/1fRJv4wjfZtWQF_sin7vfj2Z3Kg_7keIa?usp=sharing');
          }}>
            <Text style={{ textAlign: 'center' }}>
              Prosedur, JSA, dan WI
            </Text>
          </Pressable>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '100%'
  },
});
